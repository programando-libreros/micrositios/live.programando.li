function menu (e) {
  var src = e.getAttribute('data-src'),
      ife = document.getElementById('pad-container'),
      nav = document.getElementById('menu-panel').getElementsByTagName('li');
  // Changes source
  ife.src = src;

  // Removes active class
  for (var i = 0; i < nav.length; i++) {
    nav[i].classList.remove('active');
  }

  // Adds active class
  e.classList.toggle('active');
}
